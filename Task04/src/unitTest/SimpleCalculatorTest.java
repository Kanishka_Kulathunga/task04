package unitTest;

public class SimpleCalculatorTest {
	SimpleCalculator simpleCalculator;
	
	public SimpleCalculatorTest() {
		this.simpleCalculator = new SimpleCalculator();
	}
	public void testTwoPlusTwoEqualsFour() {
		UnitTestFramework.assertEquals(10.0, simpleCalculator.add(5, 5));
	}

	public void testThreePlusFiveEqualsEight() {
	 UnitTestFramework.assertTrue(simpleCalculator.add(5, 5) == 10);
	}

	public void testTenMinusFourEqualsSix() {
		UnitTestFramework.assertEquals(3.0, simpleCalculator.subs(6, 3));
	}

	public void testThreeMultipliedByFiveEqualsFifteen() {
		UnitTestFramework.assertEquals(15.0, simpleCalculator.multiply(3, 5));
	}
	
	public void testTenDividedByFiveEqualsTwo() {
		UnitTestFramework.assertEquals(5.0, simpleCalculator.divide(10, 2));
	}
	
}
