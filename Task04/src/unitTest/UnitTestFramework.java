package unitTest;

import java.lang.reflect.Method;
import java.util.*;

public class UnitTestFramework {
	private List<String> data;
	
	public UnitTestFramework() {
		data = new ArrayList<>();
	}
	
	public void testMethods(Object object) {
		String methodName;
		Method[] methods = object.getClass().getDeclaredMethods();
		for(Method method :methods) {
			methodName = method.getName();
			if(methodName.startsWith("test")) {
				try {
					method.setAccessible(true);
					method.invoke(object);
					testPass(methodName);
				}	catch (Exception e) {
					testFail(methodName,e.getCause().getMessage());
				}
			}
		}
		printData();
	}
	
	private void testPass(String methodName) {
		data.add("Pass : "+methodName);
	}
	
	private void testFail(String methodName, String error) {
		data.add("Pass : "+methodName+" "+error);
	}
	
	private void printData() {
		System.out.println("ERROR REPORT");
		for(String result : data) {
			System.out.println(result);
		}
	}

	public static void assertEquals(Object expected, Object actual) {
		if(!expected.equals(actual)) {
			throw new AssertionError("Expected : "+expected +"\t Actual : "+actual);
		}
	}
	
	public static void assertNotEquals(Object compareValue, Object actual) {
		if (compareValue.equals(actual)) {
			throw new AssertionError("Value not equal to " + compareValue);
		}
	}
	
	public static void assertNull(Object object) {
		if(object != null) {
			throw new AssertionError("Expected : Null\tActual: Not Null");
		}
	}
	
	public static void assertNotNull(Object object) {
		if(object == null) {
			throw new AssertionError("Expected : Not Null\tActual: Null");
		}
	}
	
	public static void assertTrue(boolean val) {
		if(!val) {
			throw new AssertionError("\tExpected : True\tActual: False");
		}
	}
	
	public static void assertFalse(boolean val) {
		if(val) {
			throw new AssertionError("\tExpected : False\tActual: True");
		}
	}
}