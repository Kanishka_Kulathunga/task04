package unitTest;

public class UnitTestDemo {
	public static void main(String[] args) {
		UnitTestFramework unitTestFramework = new UnitTestFramework();
		SimpleCalculatorTest simpleCalculatorTest = new SimpleCalculatorTest();
		
		unitTestFramework.testMethods(simpleCalculatorTest);
	}
	
}
