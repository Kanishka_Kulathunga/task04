package Reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Reflection03 {	//accessing private methods
	
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		Simple simple= new Simple(10,5);
		
		Method[] methods = simple.getClass().getDeclaredMethods();
		
		simple.add();
		System.out.println(simple.toString());
		
		for(Method method: methods) {
			if(method.getName().equals("substract")) {
				method.setAccessible(true);
				method.invoke(simple);
			}
		}
		System.out.println(simple.toString());
	}
}
