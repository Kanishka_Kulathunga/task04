package Reflection;

import java.lang.reflect.Field;

public class Reflection02 {	//changing private variables
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		Simple simple = new Simple(10,5);
		
		Field[] fields = simple.getClass().getDeclaredFields();
		System.out.println(simple.getNum1());
		
		for(Field field :fields){
			if(field.getName().equals("num1")){
				field.setAccessible(true);
				field.set(simple,8);
			}
		}
		
		System.out.println(simple.getNum1());
	}
}
