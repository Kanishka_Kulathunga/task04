package Reflection;

import java.lang.reflect.Field;

public class Reflection01 {	//fields
	public static void main(String[] args)throws IllegalArgumentException, IllegalAccessException {
		Simple simple = new Simple(10,5);
		
		Field[] fields = simple.getClass().getDeclaredFields();
		
		for(Field field :fields) {
			if(field.getName().equals("num2")) {
				field.setAccessible(true);
				System.out.println(field.getDouble(simple));
			}
		}
	}

}
