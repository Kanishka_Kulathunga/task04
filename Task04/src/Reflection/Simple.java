package Reflection;

public class Simple {
	private double num1;
	private double num2;
	private double answer=0;
	
	public Simple(double num1, double num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	
	public void add() {
		answer = num1+num2;
	}
	
	private void substract() {
		answer= num1-num2;
	}
	
	public double getNum1() {
		return num1;
	}
	
	private void setNum1(double num1) {
		this.num1= num1;
	}
	
	private double getNum2() {
		return num2;
	}
	
	public void setNum2(double num2) {
		this.num2=num2;
	}
	
	private double getAnswer() {
		return answer;
	}
	
	public String toString() {
		return String.format("{Answer :%.2f", answer);
	}
	
}
